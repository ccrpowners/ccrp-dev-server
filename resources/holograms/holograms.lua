--[[

	Holograms / Floating text Script by Meh
	
	Just put in the coordinates you get when standing on the ground, it's above the ground then
	By default, you only see the hologram when you are within 10m of it, to change that, edit the 10.0 infront of the "then"
	The Default holograms are at the Observatory.
	
	If you want to add a line to the hologram, just make a new Draw3DText line with the same coordinates, and edit the vertical offset.
	
	Formatting:
			Draw3DText( x, y, z  vertical offset, "text", font, text size, text size)
			
			
	To add a new hologram, copy paste this example under the existing ones, and put coordinates and text into it.
	
		if GetDistanceBetweenCoords( X, Y, Z, GetEntityCoords(GetPlayerPed(-1))) < 10.0 then
			Draw3DText( X, Y, Z,  -1.400, "TEXT", 4, 0.1, 0.1)
			Draw3DText( X, Y, Z,  -1.600, "TEXT", 4, 0.1, 0.1)
			Draw3DText( X, Y, Z,  -1.800, "TEXT", 4, 0.1, 0.1)		
		end


]]--

Citizen.CreateThread(function()
    Holograms()
end)

function Holograms()
		while true do
			Citizen.Wait(0)			
				-- Hologram No. 1
		if GetDistanceBetweenCoords( -408.50, 1163.00, 326.00, GetEntityCoords(GetPlayerPed(-1))) < 10.0 then
			Draw3DText( -408.50, 1163.00, 326.00  -1.400, "Your text", 4, 0.1, 0.1)
			Draw3DText( -408.50, 1163.00, 326.00  -1.600, "goes", 4, 0.1, 0.1)
			Draw3DText( -408.50, 1163.00, 326.00  -1.800, "here", 4, 0.1, 0.1)		
		end		
				--Hologram No. 2
		if GetDistanceBetweenCoords( -419.50, 1166.00, 326.00, GetEntityCoords(GetPlayerPed(-1))) < 10.0 then
			Draw3DText( -419.50, 1166.00, 326.00  -1.400, "Discord: discord.me/HailTheSnail", 4, 0.1, 0.1)
			Draw3DText( -419.50, 1166.00, 326.00  -1.600, "Website: www.BadExample.com", 4, 0.1, 0.1)
			Draw3DText( -419.50, 1166.00, 326.00  -1.800, "Twitter: @Example", 4, 0.1, 0.1)		
		end	
				--Mission Row 2nd floor office
		if GetDistanceBetweenCoords( 462.83, -1000.35, 35.93, GetEntityCoords(GetPlayerPed(-1))) < 3.0 then
			Draw3DText( 462.83, -1000.35, 35.93  -1.400, "Commissioner Pickles", 4, 0.1, 0.1)
			Draw3DText( 462.83, -1000.35, 35.93  -1.600, "Deputy Commissioner Fulton", 4, 0.1, 0.1)
			Draw3DText( 462.83, -1000.35, 35.93  -1.800, "", 4, 0.1, 0.1)		
		end

				--Mission Row office
				if GetDistanceBetweenCoords( 447.05, -980.61, 30.69, GetEntityCoords(GetPlayerPed(-1))) < 3.0 then
					Draw3DText( 447.05, -980.61, 30.69  -1.400, "", 4, 0.1, 0.1)
					Draw3DText( 447.05, -980.61, 30.69  -1.600, "Chief of Police", 4, 0.1, 0.1)
					Draw3DText( 447.05, -980.61, 30.69  -1.800, "", 4, 0.1, 0.1)		
				end		
			--Mission Row Shop
		if GetDistanceBetweenCoords( 468.05, -990.19, 30.69, GetEntityCoords(GetPlayerPed(-1))) < 5.0 then
			Draw3DText( 468.05, -990.19, 30.69  -1.400, "Police", 4, 0.1, 0.1)
			Draw3DText( 468.05, -990.19, 30.69  -1.600, "Shop", 4, 0.1, 0.1)
			Draw3DText( 468.05, -990.19, 30.69 -1.800, "", 4, 0.1, 0.1)		
		end

			--Job Office
		if GetDistanceBetweenCoords( 239.48, -417.41, -115.11, GetEntityCoords(GetPlayerPed(-1))) < 5.0 then
			Draw3DText( 239.48, -417.41, -115.11  -1.400, "Commercial City ", 4, 0.1, 0.1)
			Draw3DText( 239.48, -417.41, -115.11  -1.600, "Employment Office", 4, 0.1, 0.1)
			Draw3DText( 239.48, -417.41, -115.11  -1.800, "", 4, 0.1, 0.1)		
		end	

			--Classroom
			if GetDistanceBetweenCoords( 469.4, -1010.21, 26.39, GetEntityCoords(GetPlayerPed(-1))) < 5.0 then
				Draw3DText( 469.4, -1010.21, 26.39  -1.400, "", 4, 0.1, 0.1)
				Draw3DText( 469.4, -1010.21, 26.39  -1.600, "Classroom", 4, 0.1, 0.1)
				Draw3DText( 469.4, -1010.21, 26.39  -1.800, "", 4, 0.1, 0.1)		
			end			

			--Dispatch
			if GetDistanceBetweenCoords( 445.04, -989.27, 35.93, GetEntityCoords(GetPlayerPed(-1))) < 5.0 then
				Draw3DText( 445.04, -989.27, 35.93  -1.400, "", 4, 0.1, 0.1)
				Draw3DText( 445.04, -989.27, 35.93  -1.600, "Dispatch Center", 4, 0.1, 0.1)
				Draw3DText( 445.04, -989.27, 35.93  -1.800, "", 4, 0.1, 0.1)		
			end				

			--Medic Time Clock
			if GetDistanceBetweenCoords( 287.57, -1452.62, 29.97, GetEntityCoords(GetPlayerPed(-1))) < 5.0 then
				Draw3DText( 287.57, -1452.62, 29.97  -1.400, "", 4, 0.1, 0.1)
				Draw3DText( 287.57, -1452.62, 29.97  -1.600, "Time Clock", 4, 0.1, 0.1)
				Draw3DText( 287.57, -1452.62, 29.97  -1.800, "", 4, 0.1, 0.1)		
			end	
			
			--Medical Directors Office
			if GetDistanceBetweenCoords( 290.55, -1449.15, 37.91, GetEntityCoords(GetPlayerPed(-1))) < 5.0 then
				Draw3DText( 290.55, -1449.15, 37.91  -1.400, "Medical Director", 4, 0.1, 0.1)
				Draw3DText( 290.55, -1449.15, 37.91  -1.600, "T. Pickles", 4, 0.1, 0.1)
				Draw3DText( 290.55, -1449.15, 37.91  -1.800, "", 4, 0.1, 0.1)		
			end	

			--Deputy Medical Directors Office
			if GetDistanceBetweenCoords( 298.44, -1455.41, 37.97, GetEntityCoords(GetPlayerPed(-1))) < 5.0 then
				Draw3DText( 298.44, -1455.41, 37.97  -1.400, "Deputy Medical Director", 4, 0.1, 0.1)
				Draw3DText( 298.44, -1455.41, 37.97  -1.600, "W. Woodard", 4, 0.1, 0.1)
				Draw3DText( 298.44, -1455.41, 37.97  -1.800, "", 4, 0.1, 0.1)		
			end	

			--Med One Office
			if GetDistanceBetweenCoords( 316.96, -1471.13, 37.91, GetEntityCoords(GetPlayerPed(-1))) < 5.0 then
				Draw3DText( 316.96, -1471.13, 37.91  -1.400, "MED ONE", 4, 0.1, 0.1)
				Draw3DText( 316.96, -1471.13, 37.91 -1.600, "Flight Director", 4, 0.1, 0.1)
				Draw3DText( 316.96, -1471.13, 37.91  -1.800, "J. Fulton", 4, 0.1, 0.1)		
			end	

			--Dispatch Director Office
			if GetDistanceBetweenCoords( 444.76, -980.12, 26.67, GetEntityCoords(GetPlayerPed(-1))) < 5.0 then
				Draw3DText( 444.76, -980.12, 26.67  -1.400, "Dispatch Director", 4, 0.1, 0.1)
				Draw3DText( 444.76, -980.12, 26.67 -1.600, "S. Super", 4, 0.1, 0.1)
				Draw3DText( 444.76, -980.12, 26.67  -1.800, "", 4, 0.1, 0.1)		
			end	

			--DMV 1
			if GetDistanceBetweenCoords( -215.43, -1916.77, 28.82, GetEntityCoords(GetPlayerPed(-1))) < 5.0 then
				Draw3DText( -215.43, -1916.77, 28.82  -1.400, "", 4, 0.1, 0.1)
				Draw3DText( -215.43, -1916.77, 28.82  -1.600, "THIS BOOTH", 4, 0.1, 0.1)
				Draw3DText( -215.43, -1916.77, 28.82  -1.800, "OPEN", 4, 0.1, 0.1)		
			end				
			
			--DMV 2
			if GetDistanceBetweenCoords( -213.09, -1915.34, 28.82, GetEntityCoords(GetPlayerPed(-1))) < 5.0 then
				Draw3DText( -213.09, -1915.34, 28.82  -1.400, "", 4, 0.1, 0.1)
				Draw3DText( -213.09, -1915.34, 28.82  -1.600, "THIS BOOTH", 4, 0.1, 0.1)
				Draw3DText( -213.09, -1915.34, 28.82  -1.800, "CLOSED", 4, 0.1, 0.1)		
			end	
			
			--DMV 3
			if GetDistanceBetweenCoords( -210.45, -1913.5, 28.83, GetEntityCoords(GetPlayerPed(-1))) < 5.0 then
				Draw3DText( -210.45, -1913.5, 28.83  -1.400, "", 4, 0.1, 0.1)
				Draw3DText( -210.45, -1913.5, 28.83 -1.600, "THIS BOOTH", 4, 0.1, 0.1)
				Draw3DText( -210.45, -1913.5, 28.83  -1.800, "CLOSED", 4, 0.1, 0.1)		
			end				
		
			--Mission Row Parking spot 1
			if GetDistanceBetweenCoords( 438.4, -1018.3, 27.7, GetEntityCoords(GetPlayerPed(-1))) < 5.0 then
				Draw3DText( 438.4, -1018.3, 27.7  -1.400, "", 4, 0.1, 0.1)
				Draw3DText( 438.4, -1018.3, 27.7 -1.600, "Park Here", 4, 0.1, 0.1)
				Draw3DText( 438.4, -1018.3, 27.7  -1.800, "", 4, 0.1, 0.1)		
			end	

			--Mission Row Parking spot 2
			if GetDistanceBetweenCoords( -441.0, -1024.2, 28.3, GetEntityCoords(GetPlayerPed(-1))) < 5.0 then
				Draw3DText( 441.0, -1024.2, 28.3  -1.400, "", 4, 0.1, 0.1)
				Draw3DText( 441.0, -1024.2, 28.3 -1.600, "Park Here", 4, 0.1, 0.1)
				Draw3DText( 441.0, -1024.2, 28.3  -1.800, "", 4, 0.1, 0.1)		
			end	

			--Mission Row Parking spot 3
			if GetDistanceBetweenCoords( 453.5, -1022.2, 28.0, GetEntityCoords(GetPlayerPed(-1))) < 5.0 then
				Draw3DText( 453.5, -1022.2, 28.0  -1.400, "", 4, 0.1, 0.1)
				Draw3DText( 453.5, -1022.2, 28.0 -1.600, "Park Here", 4, 0.1, 0.1)
				Draw3DText( 453.5, -1022.2, 28.0  -1.800, "", 4, 0.1, 0.1)		
			end	


			--Mission Row Parking spot 4
			if GetDistanceBetweenCoords( 450.9, -1016.5, 28.1, GetEntityCoords(GetPlayerPed(-1))) < 5.0 then
				Draw3DText( 450.9, -1016.5, 28.1  -1.400, "", 4, 0.1, 0.1)
				Draw3DText( 450.9, -1016.5, 28.1 -1.600, "Park Here", 4, 0.1, 0.1)
				Draw3DText( 450.9, -1016.5, 28.1  -1.800, "", 4, 0.1, 0.1)		
			end	
	end
end

-------------------------------------------------------------------------------------------------------------------------
function Draw3DText(x,y,z,textInput,fontId,scaleX,scaleY)
         local px,py,pz=table.unpack(GetGameplayCamCoords())
         local dist = GetDistanceBetweenCoords(px,py,pz, x,y,z, 1)    
         local scale = (1/dist)*20
         local fov = (1/GetGameplayCamFov())*100
         local scale = scale*fov   
         SetTextScale(scaleX*scale, scaleY*scale)
         SetTextFont(fontId)
         SetTextProportional(1)
         SetTextColour(250, 250, 250, 255)		-- You can change the text color here
         SetTextDropshadow(1, 1, 1, 1, 255)
         SetTextEdge(2, 0, 0, 0, 150)
         SetTextDropShadow()
         SetTextOutline()
         SetTextEntry("STRING")
         SetTextCentre(1)
         AddTextComponentString(textInput)
         SetDrawOrigin(x,y,z+2, 0)
         DrawText(0.0, 0.0)
         ClearDrawOrigin()
        end
