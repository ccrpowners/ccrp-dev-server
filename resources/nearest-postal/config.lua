config = {
    text = {
        format = '~y~Nearest Postal~w~: %s (~g~%.2fm~w~)',
        -- ScriptHook PLD Position
        --posX = 0.225,
        --posY = 0.963,
        -- vMenu PLD Position
        posX = 0.160,
        posY = 0.844,
    },
    blip = {
        textFormat = 'Postal Route %s',
        color = 3, -- default 3 (light blue)
        distToDelete = 100.0, -- in meters
    }
}
