Config              = {}

Config.DrawDistance = 100.0
Config.ZoneSize     = {x = 3.0, y = 3.0, z = 0.5}
Config.MarkerColor  = {r = 100, g = 100, b = 204}
Config.MarkerType   = 1

Config.Locale       = 'en'

Config.Zones = {
	vector3(237.22, -416.79, 46.95)
}
