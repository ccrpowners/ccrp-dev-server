ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)


RegisterServerEvent('criminalAlertLoc')
AddEventHandler('criminalAlertLoc', function(x, y, z)
    local xPlayer  = ESX.GetPlayerFromId(source)
    if xPlayer.job.name ~= 'police' then
        TriggerClientEvent('criminalLocation', -1, x, y, z)
    end
end)

RegisterServerEvent('criminalAlertProgress')
AddEventHandler('criminalAlertProgress', function(street, sex, activity)
    local xPlayer  = ESX.GetPlayerFromId(source)
    if xPlayer.job.name ~= 'police' then
        TriggerClientEvent("alertFromPeds", -1, "~y~Anonymous call: ~r~Crimial activity~w~ a "..sex.." is ~g~"..activity.." ~r~On ~w~"..street)
    end
end)
