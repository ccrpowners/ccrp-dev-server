Config = {}
Config.Locale = 'en'

Config.HideMiniMapOnFoot = true --Hides the minimap while on foot
Config.EnablePVP = true -- Enable pvp
Config.DisableSeatSuffle = true -- Disable players to seat suffle
Config.NoRectagle = true -- Removes the Rectagle / Aim dot
Config.EnablePointing = true -- Enables pointing
Config.HandsUp = true -- Enable hands up
Config.DisableAmmo = true -- Disable Ammo display top right
Config.EnableIdAboveHead = False -- Id's above head
Config.EnableRPChat = true -- Rp Chat


-- Other config
Config.EnableESXIdentity = true -- only turn this on if you are using esx_identity and want to use RP names
Config.OnlyFirstname     = false