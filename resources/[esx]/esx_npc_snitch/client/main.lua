local Keys = {
	["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
	["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
	["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
	["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
	["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
	["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
	["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
	["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
	["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

ESX                  = nil
local PlayerData     = {}

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end
end)

-- Peds alert police

local timer = 1
local criminalBlipTime = 15
local target = nil
currentped = nil
local pedIsCalling = false
local timing = timer * 60000

RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
	PlayerData = xPlayer
end)

RegisterNetEvent('alertFromPeds')
AddEventHandler('alertFromPeds', function(message)
	if PlayerData.job.name == 'police' then
		notifyCops(message)
	end
end)


function notifyCops(message)
	ESX.ShowAdvancedNotification('Dispatch', 'Alert', message , 'CHAR_CALL911', 1)
end

RegisterNetEvent('criminalLocation')
AddEventHandler('criminalLocation', function(x, y, z)
	if PlayerData.job.name == 'police' then
		local alpha = 250
		local criminalBlip = AddBlipForCoord(x, y, z)
		SetBlipSprite(criminalBlip,  1)
		SetBlipColour(criminalBlip,  1)
		SetBlipAlpha(criminalBlip,  alpha)
		SetBlipAsShortRange(criminalBlip,  1)
		while alpha ~= 0 do
			Wait(criminalBlipTime * 4)
			alpha = alpha - 1
			SetBlipAlpha(criminalBlip,  alpha)
			if alpha == 0 then
				SetBlipSprite(criminalBlip,  2)
				return
			end
		end
	end
end)

Citizen.CreateThread(function()
	while true do
		Citizen.Wait(10)
		local player = GetPlayerPed(-1)
		if GetCurrentPedWeapon(player) or IsPedInMeleeCombat(player) then
			local playerloc = GetEntityCoords(player, 0)
			local handle, ped = FindFirstPed()
			repeat
				success, ped = FindNextPed(handle)
				local pos = GetEntityCoords(ped)
				local distance = GetDistanceBetweenCoords(pos.x, pos.y, pos.z, playerloc['x'], playerloc['y'], playerloc['z'], true)
				if not IsPedInAnyVehicle(GetPlayerPed(-1)) then
					if DoesEntityExist(ped)then
						if not IsPedDeadOrDying(ped) then
							if not IsPedInAnyVehicle(ped) then
								if not IsPedAPlayer(ped) then
									currentped = pos
									if distance <= 12 and ped  ~= GetPlayerPed(-1) and ped ~= oldped then
										-- Has a gun
										if GetCurrentPedWeapon(player) and not pedIsCalling then
											Citizen.Wait(1000)
											pedIsCalling = true
											oldped = ped
											TriggerEvent('animationAlert', ped)
											Citizen.Wait(10500)
											ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin, jobSkin)
												local sex = nil
												if skin.sex == 0 then
													sex = "Male"
												else
													sex = "Female"
												end
												if not IsPedDeadOrDying(ped) then
													local streetNative = Citizen.InvokeNative( 0x2EB41072B4C1E4C0, playerloc['x'], playerloc['y'], playerloc['z'], Citizen.PointerValueInt(), Citizen.PointerValueInt() )
													local street = GetStreetNameFromHashKey(streetNative)
													TriggerServerEvent('criminalAlertLoc', playerloc['x'], playerloc['y'], playerloc['z'])
													TriggerServerEvent('criminalAlertProgress', street, sex, 'carrying gun')
												end
											end)
										end
										-- is Fighthing
										if IsPedInMeleeCombat(player) and not pedIsCalling then
											target = GetMeleeTargetForPed(player)
											if IsPedInjured(target) or IsPedDeadOrDying(target) and target ~= nil then
												Citizen.Wait(2000)
												pedIsCalling = true
												TriggerEvent('animationAlert', ped)
												Citizen.Wait(10500)
												ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin, jobSkin)
													local sex = nil
													if skin.sex == 0 then
														sex = "Male"
													else
														sex = "Female"
													end
													if not IsPedDeadOrDying(ped) then
														local streetNative = Citizen.InvokeNative( 0x2EB41072B4C1E4C0, playerloc['x'], playerloc['y'], playerloc['z'], Citizen.PointerValueInt(), Citizen.PointerValueInt() )
														local street = GetStreetNameFromHashKey(streetNative)
														TriggerServerEvent('criminalAlertLoc', playerloc['x'], playerloc['y'], playerloc['z'])
														if IsPedDeadOrDying(target) then
															TriggerServerEvent('criminalAlertProgress', street, sex, 'commiting a murder')
														else
															TriggerServerEvent('criminalAlertProgress', street, sex, 'fighting')
														end
													end
												end)
											end
										end
									end
								end
							end
						end
					end
				end
			until not success
			EndFindPed(handle)
		end
	end
end)

RegisterNetEvent('animationAlert')
AddEventHandler('animationAlert', function(ped)
	RequestAnimDict("amb@world_human_stand_mobile@male@standing@call@base")
	while (not HasAnimDictLoaded("amb@world_human_stand_mobile@male@standing@call@base")) do Citizen.Wait(0) end
		TaskPlayAnim(ped,"amb@world_human_stand_mobile@male@standing@call@base","base", 1.0, 1 , 10500, 0, 1, 1, 1, 1)
		Citizen.Wait(10500)
		StopAnimTask(ped, "amb@world_human_stand_mobile@male@standing@call@base","base", 1.0)
		pedIsCalling = false
		SetPedAsNoLongerNeeded(ped)
end)
-- Peds alert police



