Config              = {}
Config              = {}

Config.JobOnly		= false -- adjust if you wish to only have a certain job(s) use mod
Config.Jobs			= {

	'mechanic',
	
}

Config.Weight		= false -- adjust to account for ESX weight or ESX limit system
Config.UseKPH		= false -- otherwise MPH
Config.DrawDistance	= 50 -- game units

Config.AlertCops	= false -- switch between sending 'intruder alert' to police
Config.UseGCPhone	= false -- switch between esx notif and gcphone for police alerts

Config.TicketCost	= 100 -- cash
Config.TicketTime	= 300 -- seconds

Config.WireCost		= 5 -- parts required to make bomb

Config.Zones = {
	
	CollectZones = {
		
		RecCenter = {
		
			Entrance = vector3(2340.87, 3128.60, 48.21),
		
			Yard = vector3(2351.30, 3053.58, 48.13)
		
		},
		
		SandyAirField = {
		
			Entrance = vector3(1759.38, 3298.74, 41.95),
			
			Yard = vector3(1754.16, 3322.97, 41.21)
			
		},
		
		MuriettaOilField = {
		
			Entrance = vector3(1569.64, -2129.88, 78.33),
			
			Yard = vector3(1507.80, -2129.14, 76.25)
			
		},
		
		HayesAuto = {
		
			Entrance = vector3(495.72, -1340.43, 29.31),
			
			Yard = vector3(456.52, -1316.19, 29.28)
			
		},
	
	},
	
	ProcessZones = {
			
		vector3(1272.81, -1711.55, 54.77),
		
		vector3(909.82, -3222.39, -98.27),
		
		vector3(1987.81, 3789.15, 32.18)
		
	},
	
	TeleInZones = {

		vector3(-3185.15, 1374.58, 19.47)
		
	},
	
	TeleOutZones = {

		vector3(902.61, -3182.32, -97.06)
		
	},
	
}

--STRINGS--
Config.NeedJack = 'You need a ~b~Car Jack~s~!'
Config.NeedJob  = 'You must have the right ~g~Job~s~!'
Config.BadCheck = 'You notice you have broken your ~b~Spec Checker~s~'
Config.BadDefus = 'You notice you have broken your ~g~Defuser~s~'
Config.GotCoin  = 'You found a ~g~Dollar~s~ coin'
Config.BombFull = 'You are too full to carry a ~r~Bomb~s~!'
Config.NeedTrig = 'You must have a ~r~Trigger~s~'
Config.NeedWire = 'You do not have enough ~y~Wires~s~'
Config.NeedCase = 'You must have a ~g~Casing~s~!'
Config.SpecFull = 'You can not carry another ~g~Spectrometer~s~!'
Config.NeedBomb = 'You must have a ~r~Bomb~s~'
Config.DeffFull = 'You can not carry another ~g~Defuser~s~!'
Config.HaveSpec = 'You already have a ~g~Spectrometer~s~!'
Config.HaveDeff = 'You already have a ~g~Defuser~s~!'
Config.NeedCash = 'You do not have enough ~g~Cash~s~!'
Config.NeedTrsh = 'You do not have enough ~r~Trash~s~'
Config.Intruder = 'An intruder has entered our property at '

Config.Confirm  = 'Confirm'
Config.Deny     = 'Deny'
Config.HighNit  = '~r~High nitrogen concentration~s~'
Config.JackLift = 'Lifting vehicle with jack'
Config.MoveBomb = 'Removing bomb'
Config.JackLowr = 'Lowering vehicle'
Config.NoDetect = 'No bomb detected'
Config.GotTick  = 'You have recieved a ~g~Ticket~s~ for the ~g~Scrap Yard~s~ for '
Config.TimeUnit = ' minutes'
Config.BomPlace = 'Placing bomb'
Config.EngBomb  = 'Make Engine Bomb'
Config.PosBomb  = 'Make Posistion Bomb'
Config.SpeBomb  = 'Make Speed Bomb'
Config.StpBomb  = 'Make Stop Bomb'
Config.SetBomb  = 'Make Seat Bomb'
Config.OneBomb  = 'You may only place one ~r~Bomb~s~ of each type at a time'
Config.DetDist  = 'Bomb Detonate Distance'
Config.DisAboveZero = 'The ~r~Distance~s~ needs to be above zero!'
Config.DisBelow1500 = 'The ~r~Distance~s~ needs to be below 1500, or the signal might not send!'
Config.DetSpeed = 'Bomb Detonate Speed'
Config.SpdAboveZero = 'The ~g~Speed~s~ needs to be above zero!'
Config.SpdBelow150 = 'The ~g~Speed~s~ needs to be below 150!'
Config.DisAboveFvie = 'The ~r~Distance~s~ needs to be above 5!'
Config.SeatList = 'driver, pass, backdrive, backpass, farbackdrive, farbackpass'
Config.TrigBomb = 'Bomb Trigger Seat'
Config.WongSeat = 'The ~b~Seat~s~ needs to be driver, pass, backdrive, backpass, farbackdriver, or farbackpass!'
Config.WastBomb = 'Why are you wasting a ~b~Seat~s~ ~r~Bomb~s~ on a driver?!'
Config.FrontArmed = 'Front passenger ~b~Seat~s~ armed!'
Config.BackLeft = 'Back left passenger ~b~Seat~s~ armed!'
Config.BackRight = 'Back right passenger ~b~Seat~s~ armed!'
Config.FarLeft = 'Far back left passenger ~b~Seat~s~ armed!'
Config.FarRight = 'Far back right passenger ~b~Seat~s~ armed!'
Config.FaceCar  = 'You must be facing a ~y~Vehicle~s~!'
Config.MakeDeff = 'Reverse Engineer'
Config.RevToDeff = 'Reverse to Defuser'
Config.TimeLeft = 'You have 30 seconds left on your ~g~Ticket~s~ time'
Config.Triggered = 'Bomb triggered'
Config.ScrapInteract = 'Press ~g~E~s~ to interact with  ~g~Scrap Yard~s~'
Config.BuyTick = 'Buy Ticket'
Config.GiveTrash = 'Give Trash'
Config.ScrapYard = 'Scrap Yard'
Config.CraftBomb = 'Press ~g~E~s~ to craft a ~r~Bomb~s~'
Config.EnterZone = 'Press ~g~E~s~ to enter location'
Config.ExitZone = 'Press ~g~E~s~ to exit location'
Config.Search = 'Search around for parts'
Config.OnGround = 'You see something on the ground'