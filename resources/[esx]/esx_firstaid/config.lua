Config                            = {}
Config.Locale                     = 'en'

Config.addTime = 60 -- time in seconds add this amount of time to the bleedouttimer
Config.firstaidWait = 600000 -- you can do firstaid after this time again
Config.pumpTime = 30 -- how often your character should do a pump (seconds for firstaid)
