resource_manifest_version '44febabe-d386-4d18-afbe-5e627f4af937'

description 'ESX Supermarket'

version '1.0.1'

files {
	'html/ui.html',
	'html/styles.css',
	'html/scripts.js',
	'html/debounce.min.js',
	'html/iransans.otf',
	

	'html/img/close.png',
	'html/img/plus.png',
	'html/img/minus.png',
	'html/img/weapons_license1.png',
	'html/img/weapons_license2.png',
	'html/img/weapons_license3.png',
	'html/img/drivers_license.png',
	'html/img/motorcycle_license.png',
	'html/img/commercial_license.png',
	'html/img/boating_license.png',
	'html/img/taxi_license.png',
	'html/img/hunting_license.png',
	'html/img/fishing_license.png',
	'html/img/marriage_license.png',
	'html/img/pilot_license.png',
	'html/img/donut.png',
	'html/img/coffee.png',
	'html/img/clip.png',
	'html/img/medikit.png',
	'html/img/armor.png',
	'html/img/diving_license.png'
}

ui_page 'html/ui.html'

client_scripts {
	'@es_extended/locale.lua',
	'locales/de.lua',
	'locales/br.lua',
	'locales/en.lua',
	'locales/fi.lua',
	'locales/fr.lua',
	'locales/es.lua',
	'locales/sv.lua',
	'locales/pl.lua',
	'locales/fa.lua',
	'config.lua',
	'client/main.lua'
}

server_scripts {
	'@es_extended/locale.lua',
	'@mysql-async/lib/MySQL.lua',
	'locales/de.lua',
	'locales/br.lua',
	'locales/en.lua',
	'locales/fi.lua',
	'locales/fr.lua',
	'locales/es.lua',
	'locales/sv.lua',
	'locales/pl.lua',
	'locales/fa.lua',
	'config.lua',
	'server/main.lua'
}

dependency 'es_extended'
