Config                            = {}

Config.DrawDistance               = 100.0

Config.Marker                     = { type = 1, x = 1.5, y = 1.5, z = 0.5, r = 102, g = 0, b = 102, a = 100, rotate = false }

Config.ReviveReward               = 700  -- revive reward, set to 0 if you don't want it enabled
Config.AntiCombatLog              = true -- enable anti-combat logging?
Config.LoadIpl                    = false -- disable if you're using fivem-ipl or other IPL loaders

Config.Locale                     = 'en'

local second = 1000
local minute = 60 * second

Config.EarlyRespawnTimer          = 2 * minute  -- Time til respawn is available
Config.BleedoutTimer              = 10 * minute -- Time til the player bleeds out

Config.EnablePlayerManagement     = true

Config.RemoveWeaponsAfterRPDeath  = true
Config.RemoveCashAfterRPDeath     = false
Config.RemoveItemsAfterRPDeath    = false

-- Let the player pay for respawning early, only if he can afford it.
Config.EarlyRespawnFine           = true
Config.EarlyRespawnFineAmount     = 50

Config.RespawnPoint = { coords = vector3(300.85, -1411.08, 37.91), heading = 273.96 }

Config.Hospitals = {

	CentralLosSantos = {

		Blip = {
			coords = vector3(307.7, -1433.4, 28.9),
			sprite = 61,
			scale  = 1.2,
			color  = 2
		},

		AmbulanceActions = {
			vector3(368.25, -1404.44, 32.96)
		},

		Pharmacies = {
			vector3(298.19, -1462.46, 28.97)
		},

		Vehicles = {
			{
				Spawner = vector3(307.7, -1433.4, 30.0),
				InsideShop = vector3(446.7, -1355.6, 43.5),
				Marker = { type = 36, x = 1.0, y = 1.0, z = 1.0, r = 100, g = 50, b = 200, a = 100, rotate = true },
				SpawnPoints = {
					{ coords = vector3(297.2, -1429.5, 29.8), heading = 227.6, radius = 4.0 },
					{ coords = vector3(294.0, -1433.1, 29.8), heading = 227.6, radius = 4.0 },
					{ coords = vector3(309.4, -1442.5, 29.8), heading = 227.6, radius = 6.0 }
				}
			}
		},

		Helicopters = {
			{
				Spawner = vector3(317.5, -1449.5, 46.5),
				InsideShop = vector3(305.6, -1419.7, 41.5),
				Marker = { type = 34, x = 1.5, y = 1.5, z = 1.5, r = 100, g = 150, b = 150, a = 100, rotate = true },
				SpawnPoints = {
					{ coords = vector3(313.5, -1465.1, 46.5), heading = 142.7, radius = 10.0 },
					{ coords = vector3(299.5, -1453.2, 46.5), heading = 142.7, radius = 10.0 }
				}
			}
		},

		FastTravels = {
			{
				From = vector3(0, 0, 0),
				To = { coords = vector3(272.8, -1358.8, 23.5), heading = 0.0 },
				Marker = { type = 1, x = 2.0, y = 2.0, z = 0.5, r = 102, g = 0, b = 102, a = 100, rotate = false }
			},

			{
				From = vector3(0, 0, 0),
				To = { coords = vector3(295.8, -1446.5, 28.9), heading = 0.0 },
				Marker = { type = 1, x = 2.0, y = 2.0, z = 0.5, r = 102, g = 0, b = 102, a = 100, rotate = false }
			},

			{
				From = vector3(0, 0, 0),
				To = { coords = vector3(333.1, -1434.9, 45.5), heading = 138.6 },
				Marker = { type = 1, x = 1.5, y = 1.5, z = 0.5, r = 102, g = 0, b = 102, a = 100, rotate = false }
			},

			{
				From = vector3(0, 0, 0),
				To = { coords = vector3(249.1, -1369.6, 23.5), heading = 0.0 },
				Marker = { type = 1, x = 2.0, y = 2.0, z = 0.5, r = 102, g = 0, b = 102, a = 100, rotate = false }
			},

			{
				From = vector3(0, 0, 0),
				To = { coords = vector3(320.9, -1478.6, 28.8), heading = 0.0 },
				Marker = { type = 1, x = 1.5, y = 1.5, z = 1.0, r = 102, g = 0, b = 102, a = 100, rotate = false }
			},

			{
				From = vector3(0, 0, 0),
				To = { coords = vector3(238.6, -1368.4, 23.5), heading = 0.0 },
				Marker = { type = 1, x = 1.5, y = 1.5, z = 1.0, r = 102, g = 0, b = 102, a = 100, rotate = false }
			}
		},

		FastTravelsPrompt = {
			{
				From = vector3(0, 0, 0),
				To = { coords = vector3(251.9, -1363.3, 38.5), heading = 0.0 },
				Marker = { type = 1, x = 1.5, y = 1.5, z = 0.5, r = 102, g = 0, b = 102, a = 100, rotate = false },
				Prompt = _U('fast_travel')
			},

			{
				From = vector3(0, 0, 0),
				To = { coords = vector3(235.4, -1372.8, 26.3), heading = 0.0 },
				Marker = { type = 1, x = 1.5, y = 1.5, z = 0.5, r = 102, g = 0, b = 102, a = 100, rotate = false },
				Prompt = _U('fast_travel')
			}
		}

	}
}   
Config.AuthorizedVehicles = {

	cadet = {
		{ model = 'ambulance3', label = 'Ambulance', price = 1}
	},

	probationary = {
		{ model = 'ambulance3', label = 'Ambulance', price = 1}
	},

	emr = {
		{ model = 'ambulance3', label = 'Ambulance', price = 1},
		{ model = 'F450', label = 'F450 Ambulance', price = 1},
		{ model = 'IA', label = 'International Ambulance', price = 1}
	},

	emt = {
		{ model = 'ambulance3', label = 'Ambulance', price = 1},
		{ model = 'F450', label = 'F450 Ambulance', price = 1},
		{ model = 'IA', label = 'International Ambulance', price = 1}
	},

	aemt = {
		{ model = 'ambulance3', label = 'Ambulance', price = 1},
		{ model = 'F450', label = 'F450 Ambulance', price = 1},
		{ model = 'IA', label = 'International Ambulance', price = 1}
	},

	paramedic = {
		{ model = 'ambulance3', label = 'Ambulance', price = 1},
		{ model = 'F450', label = 'F450 Ambulance', price = 1},
		{ model = 'IA', label = 'International Ambulance', price = 1}
	},

	medone = {
		{ model = 'ambulance3', label = 'Ambulance', price = 1},
		{ model = 'F450', label = 'F450 Ambulance', price = 1},
		{ model = 'IA', label = 'International Ambulance', price = 1},
		{ model = 'k9', label = 'K9s', price = 1}
	},

	supervisor = {
		{ model = 'ambulance3', label = 'Ambulance', price = 1},
		{ model = 'F450', label = 'F450 Ambulance', price = 1},
		{ model = 'IA', label = 'International Ambulance', price = 1}
	},

	fto = {
		{ model = 'ambulance3', label = 'Ambulance', price = 1},
		{ model = 'F450', label = 'F450 Ambulance', price = 1},
		{ model = 'IA', label = 'International Ambulance', price = 1}
	},

	medicalchief = {
		{ model = 'ambulance3', label = 'Ambulance', price = 1},
		{ model = 'F450', label = 'F450 Ambulance', price = 1},
		{ model = 'IA', label = 'International Ambulance', price = 1}
	},

	deputymedicaldirector = {
		{ model = 'ambulance3', label = 'Ambulance', price = 1},
		{ model = 'F450', label = 'F450 Ambulance', price = 1},
		{ model = 'IA', label = 'International Ambulance', price = 1},
		{ model = 'EMSC', label = 'Devils Tahoe', price = 1}
	},

	boss = {
		{ model = 'ambulance3', label = 'Ambulance', price = 1},
		{ model = 'F450', label = 'F450 Ambulance', price = 1},
		{ model = 'IA', label = 'International Ambulance', price = 1},
		{ model = 'qrv', label = 'Stickys', price = 1},
		{ model = 'k9', label = 'K9s', price = 1}
	},

}

Config.AuthorizedHelicopters = {

cadet = {},

probationary = {},

emr = {},

emt = {},

aemt = {},

paramedic = {},

medone = {
	{ model = 'supervolito', label = 'Med One', price = 1 }
},

supervisor = {},

fto = {},

medicalchief = {},

deputymedicaldirector = {},

boss = {},

}
